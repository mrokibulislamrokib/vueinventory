import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Category from '@/components/Category'
import Customer from '@/components/Customer'
import Employeer from '@/components/Employeer'
import Invoice from '@/components/Invoice'
import Order from '@/components/Order'
import Product from '@/components/Product'
import Purchase from '@/components/Purchase'
import Setting from '@/components/Setting'
import SubCategory from '@/components/SubCategory'
import DamagedProduct from '@/components/DamagedProduct'
import ProductCreate from '@/components/ProductCreate'
import OrderCreate from '@/components/OrderCreate'
import Supplier from '@/components/Supplier'
import Tax from '@/components/Tax'
import Dashboard from '@/components/Dashboard'
import PurchaseCreate from '@/components/PurchaseCreate'
import SaleCreate from '@/components/SaleCreate'
import OrderPayment from '@/components/OrderPayment'
import SalePayment from '@/components/SalePayment'
import profile from '@/components/profile'
import CustomerReport from '@/components/CustomerReport'
import PaymentReport from '@/components/PaymentReport'
import login from '@/components/login'
import Message from '@/components/Message'

Vue.use(Router)

export default new Router({
    routes: [
    	{
	      path: '/',
	      name: 'Dashboard',
	      component: Dashboard
	    },
        {
	      path: '/setting',
	      name: 'Setting',
	      component: Setting
	    },
        {
	      path: '/category',
	      name: 'Category',
	      component: Category
	    },
	    {
	      path: '/subCategory',
	      name: 'SubCategory',
	      component: SubCategory
	    },
	    {
	      path: '/product',
	      name: 'Product',
	      component: Product
	    },
	    {
	      path: '/purchase',
	      name: 'Purchase',
	      component: Purchase
	    },
	    {
	    	path:'/order',
	    	name:'Order',
	    	component:Order
	    },
	    {
	    	path:'/invoice',
	    	name:'Invoice',
	    	component:Invoice
	    },
	    {
	    	path:'/employeer',
	    	name:Employeer,
	    	component:Employeer
	    },
	    {
	    	path:'/customer',
	    	name:Customer,
	    	component:Customer
	    },
	    {
	    	path:'/damagedProduct',
	    	name:DamagedProduct,
	    	component:DamagedProduct
		    
		},
		{
	    	path:'/productCreate',
	    	name:ProductCreate,
	    	component:ProductCreate
		    
		},
		
		{
	    	path:'/supplier',
	    	name:Supplier,
	    	component:Supplier
		    
		},
		{
	    	path:'/tax',
	    	name:Tax,
	    	component:Tax
		    
		},
		{
			path:'/orderCreate',
			name:OrderCreate,
			component:OrderCreate
		},
		{
			path:'/purchaseCreate',
			name:PurchaseCreate,
			component:PurchaseCreate
		},
		{
			path:'/saleCreate',
			name:SaleCreate,
			component:SaleCreate
		},

		{
			path:'/orderPayment',
			name:OrderPayment,
			component:OrderPayment
		},

		{
			path:'/salePayment',
			name:SalePayment,
			component:SalePayment
		},
		{
			path:'/customerReport',
			name:CustomerReport,
			component:CustomerReport
		},
		{
			path:'/paymentReport',
			name:PaymentReport,
			component:PaymentReport
		},
		{
			path:'/login',
			name:login,
			component:login

		},

		{
			path:'/profile',
			name:profile,
			component:profile
		},
		{
			path:'/message',
			name:Message,
			component:Message
		}
  ]
})
