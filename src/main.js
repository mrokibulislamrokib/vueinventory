// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import App from './App'
import router from './router'


require('./assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css')
require('./assets/plugins/bootstrap/css/bootstrap.min.css')
require('./assets/plugins/font-awesome/css/font-awesome.min.css')
require('./assets/css/animate.min.css')
require('./assets/css/style-responsive.min.css')
require('./assets/css/theme/default.css')
require('./assets/css/style.css')
require('./assets/css/style-responsive.min.css')
require('./assets/css/theme/default.css')
require('./assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')
require('./assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')
require('./assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')





require('./assets/plugins/jquery/jquery-1.9.1.min')
require('./assets/plugins/jquery/jquery-migrate-1.1.0.min')
require('./assets/plugins/jquery-ui/ui/minified/jquery-ui.min')
require('./assets/plugins/bootstrap/js/bootstrap.min')
require('./assets/plugins/slimscroll/jquery.slimscroll.min')
require('./assets/plugins/jquery-cookie/jquery.cookie')
//require('./assets/plugins/DataTables/media/js/jquery.dataTables.js')
//require('./assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')
//require('./assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')
require('./assets/js/table-manage-default.demo.min')
//require('./assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')
require('./assets/js/apps.min')

Vue.config.productionTip = false

jQuery(document).ready(function($) {
	    //    App.init();
        //Dashboard.init();
      //  TableManageDefault.init();
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})

